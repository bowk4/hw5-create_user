function createNewUser() {
    let firstName = prompt("Enter a your name:")
    let lastName = prompt("Enter a your surname:")

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,
        getLogin: function() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },
        setFirstName: function(newFirstName) {
            if (typeof newFirstName === 'string') {
                this._firstName = newFirstName;
            } else {
                console.log("Неприпустиме значення для імені.");
            }
        },
        setLastName: function(newLastName) {
            if (typeof newLastName === 'string') {
                this._lastName = newLastName;
            } else {
                console.log("Неприпустиме значення для прізвища.");
            }
        }
    };

    Object.defineProperty(newUser, 'firstName', {
        writable: false
    });
    Object.defineProperty(newUser, 'lastName', {
        writable: false
    });

    return newUser;
}

let user = createNewUser();
console.log(user.getLogin());

user.setFirstName("Vasya");
user.setLastName("Petrenko");
console.log(user.getLogin());




